package example.stefan.fruits;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class FruitActivity extends AppCompatActivity {

    private ListAdapter mFruitListViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fruit);

        mFruitListViewAdapter = new FruitListViewAdapter(this);
        ListView mFruitListView = (ListView) findViewById(R.id.fruitListView);
        assert mFruitListView != null;
        mFruitListView.setAdapter(mFruitListViewAdapter);
        mFruitListView.setOnItemClickListener(new FruitListItemClickListener());
    }

    private class FruitListItemClickListener implements AdapterView.OnItemClickListener {

        private Toast t;

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String name = (String) mFruitListViewAdapter.getItem(position);

            if (t != null) t.cancel();
            t = Toast.makeText(getApplicationContext(), "You clicked on " + name.toLowerCase() +
                    ".", Toast.LENGTH_SHORT);
            t.show();
        }
    }
}
