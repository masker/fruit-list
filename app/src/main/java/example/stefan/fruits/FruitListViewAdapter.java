package example.stefan.fruits;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class FruitListViewAdapter extends BaseAdapter {

    private static final int FRUIT_COUNT = 10;
    private static final int FRUIT_SIZE = 50;
    private static final int FRUIT_COLOR_UPLIMIT = 255;

    private ArrayList<String> mFruitNames;
    private Activity context;

    public FruitListViewAdapter(Activity context) {
        this.mFruitNames = new ArrayList<>(FRUIT_COUNT);
        this.context = context;

        fillFruitNames();
    }

    private void fillFruitNames() {
        String[] strings = new String[]{"Apple", "Orange", "Pear", "Watermelon", "Blackberry",
                "Peach", "Strawberry", "Banana", "Kiwi", "Lemon"};

        mFruitNames.addAll(Arrays.asList(strings).subList(0, FRUIT_COUNT));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = new TextView(context);
        }

        Random r = new Random();
        TextView t = (TextView) convertView;

        t.setText(mFruitNames.get(position));
        t.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        t.setTextSize(FRUIT_SIZE);
        t.setTextColor(Color.argb(FRUIT_COLOR_UPLIMIT, r.nextInt(FRUIT_COLOR_UPLIMIT), r.nextInt
                (FRUIT_COLOR_UPLIMIT), r.nextInt(FRUIT_COLOR_UPLIMIT)));

        return convertView;
    }

    @Override
    public int getCount() {
        return mFruitNames.size();
    }

    @Override
    public Object getItem(int position) {
        return mFruitNames.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
